import { StatusBar } from 'expo-status-bar';
import 'react-native-gesture-handler';
import React, { Component, createRef } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  ActivityIndicator,
  Image,
  LogBox,
  Platform,
  AppState,
  AppStateStatus,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

//Lib
import { NavigationContainer } from '@react-navigation/native';
// import NetInfo from '@react-native-community/netinfo';
// import KeepAwake from 'react-native-keep-awake';
import axios from 'axios';
import _ from 'lodash';

import Config from './src/Config';
import Router from './src/Router/Router';
import { Colors, Constant, Images } from './src/Theme';
import ThemeAlert from './src/Component/ThemeAlert/ThemeAlert';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import {profileDetail, setting} from 'Container/LoginFlow/SignInScreen/Actions';
// import {showAlert, showNetWorkAlert} from 'Functions/Alerts';

// Import Screens
// import SplashScreen from './Screen/SplashScreen';
// import LoginScreen from './Screen/LoginScreen';
// import RegisterScreen from './Screen/RegisterScreen';
// import DrawerNavigatorRouts from './Screen/DrawerNavigationRoutes';

export const alertRef = createRef();

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      noInterNet: false,
      splashTimeOut: false,
    };
    const val = AsyncStorage.getItem(Constant.asyncStorageKeys.UserId)
    if(val){
      this.setState({isLogged: true});
    };
    console.log('Log value ')
    // console.disableYellowBox = true;
    // LogBox.ignoreAllLogs();
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ splashTimeOut: true });
    }, 3000);
  }

componentDidMount() {
  // const onChange = state => {
  //   if (state === 'active') {
  //     // console.log('state === active restart app');
  //   }
  // };
  // AppState.addEventListener('change', onChange);

  axios.defaults.baseURL = Config.baseURL;
  axios.defaults.headers = Config.headers;
  axios.defaults.timeout = Config.timeOut;
  // NetInfo.addEventListener(state => {
  //   Constant.commonConstant.isConnected = state.isConnected;
  //   console.log('isConnected ', Constant.commonConstant.isConnected);
  // });
  // KeepAwake.activate();
  Constant.commonConstant.emitter.addListener(
    Constant.eventListenerKeys.Login,
    () => {
      // this.getSetting();
      console.log('Login Done !!!!!');
      this.setState({ isLogged: true, isLoading: false });
    },
  );
  Constant.commonConstant.emitter.addListener(
    Constant.eventListenerKeys.Logout,
    async () => {
      await AsyncStorage.removeItem(Constant.asyncStorageKeys.UserData);
      await AsyncStorage.removeItem(Constant.asyncStorageKeys.UserId);
      await AsyncStorage.removeItem(Constant.asyncStorageKeys.UserToken);
      Constant.commonConstant.appUser = null;
      Constant.commonConstant.userToken = null;
      this.setState({ isLogged: false, isLoading: false });
    },
  );
  // this.setDefaultLocal();
  // this.getStorageValue();

  setTimeout(() => {
    this.setState({ splashTimeOut: true });
  }, 5000);
}

renderSplash = () => {
  return (
    <View style={styles.container}>
      <Image
        source={Images.appLogo}
        style={{ height: '80%', width: '90%', resizeMode: 'contain', margin: 30 }} />
      <ActivityIndicator
        animating={true}
        color='#FFFFFF'
        size='large'
        style={styles.ActivityIndicator} />
    </View>
  );
}

render() {
  const { isLogged } = this.state;
  return (
    <NavigationContainer>
      <View style={styles.containers}>
        <StatusBar
          backgroundColor={
            Platform.OS === 'android' ? Colors.transparent : Colors.white
          }
          barStyle="dark-content"
          translucent={Platform.OS === 'android' ? true : false}
        />
        {/* <SplashScreen /> */}
        <ThemeAlert ref={alertRef} />
        {this.state.splashTimeOut ? <Router isAuth={isLogged} /> : <View style={styles.container}>
          <Image
            source={Images.appLogo}
            style={{ height: '80%', width: '90%', resizeMode: 'contain', margin: 30 }} />
          <ActivityIndicator
            animating={true}
            color='#FFFFFF'
            size='large'
            style={styles.ActivityIndicator} />
        </View>}
      </View>
    </NavigationContainer>
  );
}
}

const styles = StyleSheet.create({
  containers: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  activityIndicator: {
    alignItems: 'center',
    height: '90%',
  },
});

// const Stack = createStackNavigator();

// const Auth = () => {
//   // Stack Navigator for Login and Sign up Screen
//   return (
//     <Stack.Navigator initialRouteName="LoginScreen">
//       <Stack.Screen
//         name="LoginScreen"
//         component={LoginScreen}
//         options={{headerShown: false}}
//       />
//       <Stack.Screen
//         name="RegisterScreen"
//         component={RegisterScreen}
//         options={{
//           title: 'Register', //Set Header Title
//           headerStyle: {
//             backgroundColor: '#307ecc', //Set Header color
//           },
//           headerTintColor: '#fff', //Set Header text color
//           headerTitleStyle: {
//             fontWeight: 'bold', //Set Header text style
//           },
//         }}
//       />
//     </Stack.Navigator>
//   );
// };

// const App = () => {
//   return (
//     <NavigationContainer>
//       <StatusBar backgroundColor= '#106ecc' style= 'light' />
//       <Stack.Navigator initialRouteName="SplashScreen">
//         {/* SplashScreen which will come once for 5 Seconds */}
//         <Stack.Screen
//           name="SplashScreen"
//           component={SplashScreen}
//           // Hiding header for Splash Screen
//           options={{headerShown: false}}
//         />
//         {/* Auth Navigator: Include Login and Signup */}
//         <Stack.Screen
//           name="Auth"
//           component={Auth}
//           options={{headerShown: false}}
//         />
//         {/* Navigation Drawer as a landing page */}
//         <Stack.Screen
//           name="DrawerNavigatorRouts"
//           component={DrawerNavigatorRouts}
//           // Hiding header for Navigation Drawer
//           options={{headerShown: false}}
//         />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// };

// export default App;
