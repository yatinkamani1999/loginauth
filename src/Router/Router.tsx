/* eslint-disable prettier/prettier */
import React from 'react';
import { BackHandler, Platform, TouchableOpacity, Image } from 'react-native';

// Lib
import {
  Router,
  Scene,
  Stack,
  Drawer,
  Tabs,
  Actions,
} from 'react-native-router-flux';
// Mics Constants
import { Screens, Colors, Responsive, Images } from '../Theme';

// Components
// import { TabIcon } from '../Component/TabIcon/TabIcon';

// Login and signUp flow Screen
import SignInScreen from '../Container/LoginFlow/SignInScreen/SignInScreen';
import SignUpScreen from '../Container/LoginFlow/SignUpScreen/SignUpScreen';

import HomeScreen from '../Container/MainScreen/Home';

const onBackPress = () => {
  BackHandler.exitApp();
};

const barStyle =
  Platform.OS === 'ios'
    ? {
      backgroundColor: Colors.shadowColor1,
      bottom: 0,
      height: Responsive.heightPercentageToDP(60),
      borderTopWidth: 0,
      position: 'absolute',
    }
    : {
      backgroundColor: Colors.shadowColor1,
      height:
        Platform.OS === 'android'
          ? Responsive.heightPercentageToDP(65)
          : Responsive.heightPercentageToDP(0),
      paddingBottom: Responsive.heightPercentageToDP(0),
      borderTopWidth: 0,
      position: 'absolute',
      bottom: 0,
    };

const MyTransitionSpec = {
  duration: 1000,
  // easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
  // timing: Animated.timing,
};

// Main Navigation Flow
export default function RouterComponent({ isAuth }) {
  return (
    <Router backAndroidHandler={onBackPress}>
      <Stack hideNavBar>
        <Scene hideNavBar key={Screens.AuthKey}>
          <Scene
            key={Screens.SignInScreen}
            component={SignInScreen}
            initial={true}
          />
          <Scene key={Screens.SignUpScreen} component={SignUpScreen} />
        </Scene>
        <Scene hideNavBar key={Screens.RootKey} initial={isAuth}>
          <Scene
            key={Screens.Home}
            component={HomeScreen}
            initial={true}
          />
        </Scene>
      </Stack>
    </Router>
  );
}
