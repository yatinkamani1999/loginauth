import {Actions} from 'react-native-router-flux';

export const push = (destinationScene, props) => {
  console.log('push', destinationScene, Actions.currentScene, props);
  if (Actions.currentScene === destinationScene) {
    return;
  }
  return Actions[destinationScene](props);
  // return Actions.push(destinationScene, props);
};
// Pop to last navigate key.
export const pop = () => {
  return Actions.pop();
};
export const replace = (destinationScene, props) => {
  return Actions.replace(destinationScene, props);
};
// Open drawerOpen
export const drawerOpen = () => {
  return Actions.drawerOpen();
};
// Close drawerClose
export const drawerClose = () => {
  return Actions.drawerClose();
};
// Pop to specific screen.
export const popTo = (sceneKey, props) => {
  return Actions.popTo(sceneKey, props);
};
// used to switch to a new tab. For Tabs only.

export const jump = (sceneKey, props) => {
  console.log('jump', sceneKey, props);
  return Actions.jump(sceneKey, props);
};
