/* eslint-disable prettier/prettier */
import { Dimensions } from 'react-native';
import { EventEmitter } from 'fbemitter';
import Config from '../Config';
import { hideAlert, showAlert } from '../Functions/Alerts';
import { Colors, Constant } from './index';

// import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-async-storage/async-storage';

// import { strings } from 'Resources/locales/i18n';
const { width, height } = Dimensions.get('window');

export const commonConstant = {
  scrWidth: width,
  scrHeight: height,
  userToken: '433fc02b-b5c1-4bd8-836a-618cdc31c492',
  emitter: new EventEmitter(),
  appUser: null,
  UserId: '',
  userLoginData: null,
  // hasNotch: DeviceInfo.hasNotch(),
  hasNotch: true,
  isConnected: true,
  // googlePlaceAPI: 'AIzaSyBeLQZYcfGKFbZyCFBXl700jtMHynrVosU',
};
export const eventListenerKeys = {
  Login: 'Login',
  Logout: 'Logout',
};

export const asyncStorageKeys = {
  UserData: 'UserData',
  UserId: 'UserId',
  UserToken: 'Token',
  userLoginData: 'LoginData',
};

export const getHeaderWithAuthToken = () => {
  const newHeaders = Object.assign({}, Config.headers, {
    Authorization: `Bearer ${commonConstant.userToken}`,
  });
  return newHeaders;
};

export function errorHandle(response) {
  let buttons = [
    {
      id: 1,
      label: 'Ok',
      action: () => {
        hideAlert();
        setTimeout(() => {
          commonConstant.emitter.emit(eventListenerKeys.Logout);
        }, 300);
      },
      labelColor: Colors.ThemeColorBlue,
    },
  ];
  if (response && response.data) {
    if (response.status === 401) {
      // showAlertWithOkBtn(response.data.message).then(() => {
      //   setTimeout(() => {
      //     commonConstant.emitter.emit(eventListenerKeys.Logout);
      //   }, 300);
      // });
      showAlert(response.data.messages, buttons);
    } else if (response.status === 500) {
      showAlert('Server Error...! Please try again');
    } else if (response.data.message) {
      showAlert(response.data.message);
    } else {
      showAlert('Please try again');
    }
  } else {
    // showAlert('Please try again');
  }
}

export const API = {
  LoginApi: '/api/authaccount/login',
  register: '/api/authaccount/registration',
};

export default {
  commonConstant,
  API,
  eventListenerKeys,
  asyncStorageKeys,
  getHeaderWithAuthToken,
  errorHandle,
};
