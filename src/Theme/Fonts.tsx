const fonts = {
  // ThemeRegular: 'NunitoSans-Regular',
  // ThemeBlack: 'NunitoSans-Black',
  // ThemeBlackItalic: 'NunitoSans-BlackItalic',
  // ThemeBold: 'NunitoSans-Bold',
  // ThemeBoldItalic: 'NunitoSans-BoldItalic',
  // ThemeExtraBold: 'NunitoSans-ExtraBold',
  // ThemeExtraBoldItalic: 'NunitoSans-ExtraBoldItalic',
  // ThemeExtraLight: 'NunitoSans-ExtraLight',
  // ThemeExtraLightItalic: 'NunitoSans-ExtraLightItalic',
  // ThemeItalic: 'NunitoSans-Italic',
  // ThemeLight: 'NunitoSans-Light',
  // ThemeLightItalic: 'NunitoSans-LightItalic',
  // ThemeSemiBold: 'NunitoSans-SemiBold',
  // ThemeSemiBoldItalic: 'NunitoSans-SemiBoldItalic',
};
export default {
  fonts,
};
