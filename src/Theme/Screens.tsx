export default {
  // LoginFlow Screen
  AuthKey: 'auth',
  SignInScreen: 'SignInScreen',
  SignUpScreen: 'SignUpScreen',

  // MainFlow Screen
  RootKey: 'root',
  Home: 'Home',

};
