import React, {useState, useEffect, useRef} from 'react';
import {Text, Image, View} from 'react-native';
// Lib
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import Icon from 'react-native-vector-icons/FontAwesome5Pro'
import axios from 'axios';
// Component
import ThemeTextInput from '../../../Component/ThemeTextInput/ThemeTextInput';
import ThemeButton from '../../../Component/ThemeButton/ThemeButton';
import Loader from '../../../Component/Loader';
// Mics
import {validateEmail, validatePassword} from '../../../Functions/Validate';
import {Colors, Constant, Images, Screens} from '../../../Theme';
import {getHeaderWithAuthToken} from '../../../Theme/Constant';
import {hideAlert, showAlert} from '../../../Functions/Alerts';
import {LoginCall} from './Actions';
// import {strings} from 'Resources/locales/i18n';
import {push} from '../../../Theme/Actions';
import {styles} from './SignInScreenStyle';

const SignInScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isRemember, setIsRemember] = useState(false);
  const [submitPressed, setSubmitPressed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [errEmail, setErrEmail] = useState('');
  const [errPassword, setErrPassword] = useState('');

  const emailTextInput = useRef(null);
  const passwordTextInput = useRef(null);

  useEffect(() => {
    navigation.addListener('didFocus', async () => {
      console.log('didFocus');
    
    });
  });

  // Render Method
  const onPressLogin = async () => {
    var isValid = true;
    setSubmitPressed(true);
    if (!email) {
      setErrEmail('enter email');
      isValid = false;
    } else if (!validateEmail(email)) {
      setErrEmail('enter valid Email');
      isValid = false;
    }
    if (!password) {
      setErrPassword(('enter password'));
      isValid = false;
    } else if (!validatePassword(password)) {
      setErrPassword('enter valid password');
      isValid = false;
    }
    if (isValid) {
      setIsLoading(true);
      const param = {
        email: email,
        password: password,
      };
      const response = await LoginCall(param);
      console.log('Login response ==> ' + JSON.stringify(response.data), null, 2);
      if (response && response.status === 200 && response.data) {
        const userToken = response.data.token;
        console.log('app user Name ==>', response.data.data.Name);
        // if (isRemember) {
        //   const temp = {
        //     email: email,
        //     password: password,
        //   };
        //   Constant.commonConstant.userLoginData = temp;
        //   // await AsyncStorage.setItem(
        //   //   Constant.asyncStorageKeys.isLoginDataRemember,
        //   //   true,
        //   // );
        //   await AsyncStorage.setItem(
        //     Constant.asyncStorageKeys.userLoginData,
        //     JSON.stringify(temp),
        //   );
        // } else {
        //   const temp = {};
        //   Constant.commonConstant.userLoginData = temp;
        //   await AsyncStorage.setItem(
        //     Constant.asyncStorageKeys.userLoginData,
        //     JSON.stringify(temp),
        //   );
        // }

        // Constant.commonConstant.userToken = userToken;
        // Constant.commonConstant.UserId = response.data.userid;
        // await AsyncStorage.setItem(
        //   Constant.asyncStorageKeys.UserToken,
        //   userToken,
        // );
        await AsyncStorage.setItem(
          Constant.asyncStorageKeys.UserId,
          '' + response.data.data.Id,
        );
           await AsyncStorage.setItem(
            Constant.asyncStorageKeys.userLoginData,
            JSON.stringify(response.data.data),
          );
        // axios.defaults.headers = getHeaderWithAuthToken();
        // await getProfile();
        Constant.commonConstant.emitter.emit(Constant.eventListenerKeys.Login);
        const val = await AsyncStorage.getItem(Constant.asyncStorageKeys.UserId)
        console.log('Login Success fully', val);
        
        console.log('Login Success fully');
      } else if (response && response.status === 401 && response.data) {
        setIsLoading(false);
        let buttons = [
          {
            id: 1,
            label: 'Ok',
            action: () => {
              hideAlert();
              // generateOTP();
            },
            labelColor: Colors.ThemeColorBlue,
          },
        ];
        let data = response.data;
        if (data.hasOwnProperty('messages')) {
          if (data.messages[0] === 'Provided Credentials are invalid.') {
            showAlert('login error');
          } else if (data.messages[0] === 'Authentication Failed.') {
            showAlert(data.messages[0]);
          } else if (data.messages[0] === 'Invalid Email') {
            showAlert(data.messages[0]);
          } else if (data.messages[0] === 'Invalid Password') {
            showAlert(data.messages[0]);
          } else {
            showAlert(data.messages[0], buttons);
          }
        }
      } else if (response && response.status === 422 && response.data) {
        setIsLoading(false);
        let data = response.data.data;
        if (data.hasOwnProperty('email')) {
          showAlert(data.email[0]);
        } else if (data.hasOwnProperty('password')) {
          showAlert(data.password[0]);
        } else {
          showAlert(strings('login.loginErr1'));
        }
      } else {
        setIsLoading(false);
        Constant.errorHandle(response);
      }
    }
  };

  const onPressSignUp = () => {
    push(Screens.SignUpScreen);
  };

  const onChangeEmail = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!text) {
        errorMessage = 'Enter email';
      } else if (!validateEmail(text)) {
        errorMessage = ('enter valid Email');
      }
    }
    setEmail(text);
    setErrEmail(errorMessage);
  };
  const onChangePassword = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!password) {
        errorMessage = ('enter password');
      } else if (!validatePassword(password)) {
        errorMessage = ('enter valid Password');
      }
    }
    setPassword(text);
    setErrPassword(errorMessage);
  };

  const onPressRemember = () => {
    setIsRemember(!isRemember);
  };

  const focusPassword = () => {
    if (passwordTextInput.current !== null) {
      passwordTextInput.current.focus();
    }
  };

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{paddingBottom: 70}}
      showsVerticalScrollIndicator={false}
      style={styles.container}
      keyboardShouldPersistTaps="handled">
      <Loader isLoading={isLoading} />
      <Image source={Images.appLogo} style={styles.logoStyle} />
      <Text style={styles.textSignIn}>{'SignIn'}</Text>
      <Text style={styles.textLabel}>{'SignIn the acount for your email or password'}</Text>
      <ThemeTextInput
        onChangeText={(text: string) => onChangeEmail(text)}
        containerStyle={{}}
        label={'Email'}
        value={email}
        icon={'envelope'}
        keyboardType={'email-address'}
        placeholder={'Enter Email'}
        error={errEmail}
        onSubmitEditing={focusPassword}
        returnKeyType={'next'}
        autoCapitalize={'none'}
        inputRef={input => {
          emailTextInput.current = input;
        }}
      />
      <ThemeTextInput
        onChangeText={(text: string) => onChangePassword(text)}
        containerStyle={{}}
        label={'Password'}
        value={password}
        error={errPassword}
        icon={'lock'}
        keyboardType={'default'}
        placeholder={'Enter Password'}
        onSubmitEditing={onPressLogin}
        returnKeyType={'done'}
        autoCapitalize={'none'}
        inputRef={input => {
          passwordTextInput.current = input;
        }}
        isPasswordShow={true}
      />
      {/* <View style={styles.flexViewStyle}>
        <View style={styles.rememberView}>
          <Icon
            name={isRemember ? 'check-square' : 'square'}
            size={24}
            color={isRemember ? Colors.ThemeColorBlue : Colors.PlaceHolderColor}
            onPress={onPressRemember}
          />
          <Text style={styles.rememberTexts}>
            {'RememberMe'}
          </Text>
        </View>
        <Text style={styles.blueTexts} onPress={onPressForgotPassword}>
          {'login.forgotPwd')}
        </Text>
      </View> */}
      <ThemeButton
        textValue={'Login'}
        onPress={onPressLogin}
        containerStyle={styles.logInStyle}
      />
      <View style={styles.bottomLabelStyle}>
        <Text style={styles.textLabelStyle}>
          {'if you don\'t have an account ?'}
        </Text>
        <Text style={styles.textSignUpStyle} onPress={onPressSignUp}>
          {'SignUp'}
        </Text>
      </View>
    </KeyboardAwareScrollView>
  );
};
export default SignInScreen;
