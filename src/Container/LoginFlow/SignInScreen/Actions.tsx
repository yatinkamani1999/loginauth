import Axios from 'axios';
import {showNetWorkAlert} from '../../../Functions/Alerts';
import {Constant} from '../../../Theme';

export function LoginCall(data) {
  if (Constant.commonConstant.isConnected) {
    return Axios.post(Constant.API.LoginApi, data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        console.log('error ', error);
        return error.response;
      });
  } else {
    showNetWorkAlert();
  }
}
