/* eslint-disable prettier/prettier */
import { Platform, StyleSheet } from 'react-native';
import { Colors, Fonts, Responsive } from '../../../Theme';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingTop: Responsive.heightPercentageToDP(50),
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 18,
    // marginTop:
    //   Platform.OS === 'ios'
    //     ? Responsive.heightPercentageToDP(10)
    //     : Responsive.heightPercentageToDP(10),
  },
  barIconStyle: {
    height: Responsive.heightPercentageToDP(24),
    width: Responsive.widthPercentageToDP(24),
    marginStart: Responsive.widthPercentageToDP(21),
    resizeMode: 'contain',
    position: 'absolute',
    left: 0,
  },
  logInStyle: {
    marginTop: Responsive.heightPercentageToDP(31),
  },
  bottomLabelStyle: {
    marginTop: Responsive.heightPercentageToDP(15),
    // width: Responsive.widthPercentageToDP(170),
    padding: 5,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    // alignContent: 'center',
    // justifyContent: 'center',
    borderColor: Colors.ThemeBorder,
    // borderWidth: 1,
    // borderRadius: 23,
  },
  logoStyle: {
    alignSelf: 'center',
    resizeMode: 'contain',
    height: Responsive.heightPercentageToDP(150),
    width: Responsive.widthPercentageToDP(150),
    marginTop: Responsive.heightPercentageToDP(50),
    marginBottom: Responsive.heightPercentageToDP(30),
  },
  textSignIn: {
    color: Colors.blackShade36,
    fontSize: Responsive.convertFontScale(32),
    fontWeight: '800',
    textAlign: 'center',
    marginBottom: Responsive.heightPercentageToDP(13),
  },
  textLabel: {
    color: Colors.blackShade36,
    fontSize: Responsive.convertFontScale(14),
    fontWeight: '400',
    textAlign: 'center',
    opacity: 0.5,
    marginBottom: Responsive.heightPercentageToDP(20),
  },
  flexViewStyle: {
    flexDirection: 'row',
    marginBottom: Responsive.heightPercentageToDP(20),
    marginTop: Responsive.heightPercentageToDP(25),
    marginHorizontal: Responsive.widthPercentageToDP(44),
    justifyContent: 'space-between',
  },
  rememberView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rememberTexts: {
    fontSize: Responsive.convertFontScale(14),
    fontWeight: '400',
    color: Colors.PlaceHolderColor,
    marginStart: 10,
  },
  blueTexts: {
    fontSize: Responsive.convertFontScale(14),
    fontWeight: '400',
    color: Colors.ThemeColorBlue,
    alignSelf: 'flex-end',
  },
  textLabelStyle: {
    color: Colors.blackShade3B,
    fontSize: Responsive.convertFontScale(12),
    fontWeight: '400',
    textAlign: 'center',
  },
  textSignUpStyle: {
    color: Colors.ThemeColorBlue,
    fontSize: Responsive.convertFontScale(12),
    fontWeight: '700',
    textAlign: 'center',
  },
});
