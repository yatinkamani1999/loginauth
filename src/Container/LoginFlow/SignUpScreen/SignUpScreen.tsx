/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useState, useRef, useEffect} from 'react';
import {Text, Image, View, Keyboard} from 'react-native';
// Lib
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
// Component
import ThemeTextInput from '../../../Component/ThemeTextInput/ThemeTextInput';
import ThemeButton from '../../../Component/ThemeButton/ThemeButton';
import Loader from '../../../Component/Loader';
// Mics
import {validateEmail, validatePassword} from '../../../Functions/Validate';
import {Colors, Constant, Images, Screens} from '../../../Theme';
import {hideAlert, showAlert} from '../../../Functions/Alerts';
import {pop, push} from '../../../Theme/Actions';
import {styles} from './SignUpScreenStyle';
import {signUpCall} from './Actions';

const SignUpScreen = refreshData => {
  const [confirmPassword, setConfirmPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [userId, setUserId] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [submitPressed, setSubmitPressed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [errUserId, setErrUserId] = useState('');
  const [errFirstName, setErrFirstName] = useState('');
  const [errLastName, setErrLastName] = useState('');
  const [errEmail, setErrEmail] = useState('');
  const [errPassword, setErrPassword] = useState('');
  const [errConfirmPassword, setErrConfirmPassword] = useState('');

  const emailTextInput = useRef(null);
  const passTextInput = useRef(null);
  const confirmPassTextInput = useRef(null);
  const userIdTextInput = useRef(null);
  const lastNameTextInput = useRef(null);

  // Mics Method
  const onChangeFirstName = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!text) {
        errorMessage = 'Enter first name';
      }
    }
    setFirstName(text);
    setErrFirstName(errorMessage);
  };
  const onChangeLastName = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!text) {
        errorMessage = 'Enter last Name';
      }
    }
    setLastName(text);
    setErrLastName(errorMessage);
  };
  const onChangeUserId = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!text) {
        errorMessage = 'user Id';
      }
    }
    setUserId(text);
    setErrUserId(errorMessage);
  };
  const onChangeEmail = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!text) {
        errorMessage = 'enter email';
      } else if (!validateEmail(text)) {
        errorMessage = 'enter valid Email';
      }
    }
    setEmail(text);
    setErrEmail(errorMessage);
  };
  const onChangePassword = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!password) {
        errorMessage = 'enter password';
      } else if (!validatePassword(password)) {
        errorMessage = 'enter valid Password';
      }
    }
    setPassword(text);
    setErrPassword(errorMessage);
  };
  const onChangeConfirmPassword = (text: string) => {
    var errorMessage = '';
    if (submitPressed) {
      if (!confirmPassword) {
        errorMessage = 'enter password';
      } else if (text !== password) {
        errorMessage = 'enter Valid Confirm Password';
      }
    }
    setConfirmPassword(text);
    setErrConfirmPassword(errorMessage);
  };

  // Render Method
  const onPressSignUp = async () => {
    Keyboard.dismiss();
    var isValid = true;
    setSubmitPressed(true);
    if (!firstName) {
      setErrFirstName('first Name');
      isValid = false;
    }
    if (!lastName) {
      setErrLastName('enter last Name');
      isValid = false;
    }
    if (!userId) {
      setErrUserId('enter user name');
      isValid = false;
    }
    if (!email) {
      setErrEmail('enter email');
      isValid = false;
    } else if (!validateEmail(email)) {
      setErrEmail('enter valid Email');
      isValid = false;
    }
    if (!password) {
      setErrPassword('enter password');
      isValid = false;
    } else if (!validatePassword(password)) {
      setErrPassword('enter valid Password');
      isValid = false;
    }
    if (!confirmPassword) {
      setErrConfirmPassword('enter password');
      isValid = false;
    } else if (confirmPassword !== password) {
      setErrConfirmPassword('enter valid Confirm Password');
      isValid = false;
    }
    if (isValid) {
      setIsLoading(true);
      const param = {
        name : firstName +" "+lastName,
        // lastName: lastName,
        // userName: userId,
        email: email,
        password: password,
      };
      const response = await signUpCall(param);
      console.log('Login response ==> ' + JSON.stringify(response));
      if (response && response.status === 200 && response.data) {
        let buttons = [
          {
            id: 1,
            label: 'Ok',
            action: () => {
              hideAlert();
              pop();
              // push(Screens.OTPScreen, {email: email, isFromSignUp: true});
            },
            labelColor: Colors.ThemeColorBlue,
          },
        ];
        const temp = {};
        Constant.commonConstant.userLoginData = temp;
        await AsyncStorage.setItem(
          Constant.asyncStorageKeys.userLoginData,
          JSON.stringify(temp),
        );
        showAlert(
          'User register successfully! \n Login with your Email and password.',
          buttons,
        );
      } else if (response && response.status === 400 && response.data) {
        let data = response.data.errors;
        if (data.hasOwnProperty('UserName')) {
          showAlert(data.UserName[0]);
        } else if (data.hasOwnProperty('Email')) {
          showAlert(data.Email[0]);
        } else if (data.hasOwnProperty('password')) {
          showAlert(data.password[0]);
        } else {
          showAlert('tryAgain');
        }
      } else if (response && response.status === 500 && response.data) {
        let data = response.data.messages[0];
        console.log('aaaa 500', data.length);
        showAlert(data);
      } else {
        Constant.errorHandle(response);
      }

      setIsLoading(false);
      // Constant.commonConstant.emitter.emit(Constant.eventListenerKeys.Login);
    }
  };
  const onPressSignIn = async () => {
    pop();
  };

  const focusLastName = () => {
    if (lastNameTextInput.current !== null) {
      lastNameTextInput.current.focus();
    }
  };
  const focusUserId = () => {
    if (userIdTextInput.current !== null) {
      userIdTextInput.current.focus();
    }
  };
  const focusEmail = () => {
    if (emailTextInput.current !== null) {
      emailTextInput.current.focus();
    }
  };
  const focusPassword = () => {
    if (passTextInput.current !== null) {
      passTextInput.current.focus();
    }
  };
  const focusConfirmPassword = () => {
    if (confirmPassTextInput.current !== null) {
      confirmPassTextInput.current.focus();
    }
  };

  return (
    <KeyboardAwareScrollView
      showsVerticalScrollIndicator={false}
      style={styles.container}
      keyboardShouldPersistTaps="always">
      <Loader isLoading={isLoading} />
      <View style={styles.innerContainer}>
        <Image source={Images.appLogo} style={styles.logoStyle} />
        <Text style={styles.textSignIn}>{'SignUp'}</Text>
        <Text style={styles.textLabel}>{'create new account for our platform'}</Text>
        <ThemeTextInput
          onChangeText={(text: string) => onChangeFirstName(text)}
          containerStyle={{}}
          value={firstName}
          icon={'user'}
          error={errFirstName}
          keyboardType={'default'}
          placeholder={'Enter your FirstName'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
          onSubmitEditing={focusLastName}
        />
        <ThemeTextInput
          onChangeText={(text: string) => onChangeLastName(text)}
          containerStyle={{}}
          value={lastName}
          icon={'user'}
          error={errLastName}
          keyboardType={'default'}
          placeholder={'Enter last Name'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
          inputRef={input => {
            lastNameTextInput.current = input;
          }}
          onSubmitEditing={focusUserId}
        />
        <ThemeTextInput
          onChangeText={(text: string) => onChangeUserId(text)}
          containerStyle={{}}
          value={userId}
          error={errUserId}
          icon={'user-shield'}
          keyboardType={'email-address'}
          placeholder={'Enter username'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
          inputRef={input => {
            userIdTextInput.current = input;
          }}
          onSubmitEditing={focusEmail}
        />
        <ThemeTextInput
          onChangeText={(text: string) => onChangeEmail(text)}
          containerStyle={{}}
          value={email}
          error={errEmail}
          icon={'envelope'}
          keyboardType={'email-address'}
          placeholder={'Enter Email'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
          inputRef={input => {
            emailTextInput.current = input;
          }}
          onSubmitEditing={focusPassword}
        />
        <ThemeTextInput
          onChangeText={(text: string) => onChangePassword(text)}
          containerStyle={{}}
          value={password}
          error={errPassword}
          icon={'lock'}
          keyboardType={'default'}
          placeholder={'Enter password'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
          isPasswordShow={true}
          inputRef={input => {
            passTextInput.current = input;
          }}
          onSubmitEditing={focusConfirmPassword}
        />
        <ThemeTextInput
          onChangeText={(text: string) => onChangeConfirmPassword(text)}
          containerStyle={{}}
          value={confirmPassword}
          error={errConfirmPassword}
          icon={'lock'}
          keyboardType={'default'}
          placeholder={'Enter confirm Password'}
          onSubmitEditing={onPressSignUp}
          returnKeyType={'done'}
          autoCapitalize={'none'}
          isPasswordShow={true}
          inputRef={input => {
            confirmPassTextInput.current = input;
          }}
        />
        <ThemeButton
          textValue={'SignUp'}
          onPress={onPressSignUp}
          containerStyle={styles.logInStyle}
        />
        <View style={styles.bottomLabelStyle}>
          <Text style={styles.textLabelStyle}>{'if you have already an Account ?'}</Text>
          <Text style={styles.textSignUpStyle} onPress={onPressSignIn}>
            {'SignIn'}
          </Text>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};
export default SignUpScreen;
