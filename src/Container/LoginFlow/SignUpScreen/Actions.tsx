import Axios from 'axios';
import { showNetWorkAlert } from '../../../Functions/Alerts';
import { Constant } from '../../../Theme';

export async function signUpCall(data) {
  console.log('signUpCall data = ', data);
  if (Constant.commonConstant.isConnected) {
    try {
      const response = await Axios.post(Constant.API.register, data);
      return response;
    } catch (error) {
      console.log('error ', error);
      return error.response;
    }
  } else {
    showNetWorkAlert();
  }
}
