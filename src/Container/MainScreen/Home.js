import React, {useState, useEffect} from 'react';
import { ActivityIndicator, Image, StatusBar, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {Images, Constant} from "../../Theme"
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({navigation}) => {
    
    const [fullname, setFullName] = useState('');
    const [email, setEmail] = useState('');

    useEffect(() => {
        async function getdata(){
            let d = await AsyncStorage.getItem(Constant.asyncStorageKeys.userLoginData);
            const data = JSON.parse(d);
            setFullName(data.Name);
            setEmail(data.Email)
            console.log('Print Data '+data);
        }
        getdata();
    },[]);


    const onLogout = () => {
        Constant.commonConstant.emitter.emit(Constant.eventListenerKeys.Logout);
    }    

    return (
        
        <View style={styles.container}>
            <Text>
                Login User Name : {fullname}
            </Text>
            <Text style={{marginBottom: 30, paddingVertical: 10}}>
                Login User Email : {email}
            </Text>
            <TouchableOpacity onPress={onLogout}>
                <Text
                style={{
                    backgroundColor: 'red',
                    borderRadius: 10,
                    shadowOffset: 1,
                    shadowColor: 'black',
                    paddingHorizontal: 30,
                    paddingVertical: 10,
                    fontSize: 20,
                    color: 'white',
                    fontWeight: 'bold'
                }}>
                    Logout
                </Text>

            </TouchableOpacity>
        </View>
    );
};

export default Home;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff',
    },
    activityIndicator: {
      alignItems: 'center',
      height: '90%',
    },
  });