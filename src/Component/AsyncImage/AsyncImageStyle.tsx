import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  imageResizeMode: {
    resizeMode: 'cover',
  },
  imageStyleView: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  centerStyle: {justifyContent: 'center', alignItems: 'center'},
  centerStyle1: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 3,
    backgroundColor: 'green',
  },
  userProfile: {height: '100%', width: '100%', borderRadius: 50},
  positionStyle: {
    position: 'absolute',
  },
});

export default {styles};
