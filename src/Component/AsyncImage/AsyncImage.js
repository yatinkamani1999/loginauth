/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Image, ActivityIndicator } from 'react-native';
// Lib
import FastImage from 'react-native-fast-image';
import PropTypes from 'prop-types';
// Mics
import { styles } from './AsyncImageStyle';
import { Colors } from '../../Theme';

export default class AsyncImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeHolderShow: false,
      isError: false,
      blurRadius: 0,
      isLoading: true,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.blurRadius !== prevState.blurRadius) {
      return { blurRadius: nextProps.blurRadius };
    } else if (nextProps.source !== prevState.source) {
      return { source: nextProps.source, isError: false };
    }
    return null;
  }

  render() {
    const { style, source, placeHolder, resizeMode } = this.props;
    const { placeHolderShow, blurRadius, isLoading } = this.state;
    const mainSource = placeHolderShow ? placeHolder : source;
    return (
      <View style={[style, styles.centerStyle]}>
        <FastImage
          key={new Date()}
          blurRadius={blurRadius}
          source={mainSource}
          resizeMode={resizeMode}
          // key={placeHolderShow ? '' : mainSource.uri}
          style={[style, styles.imageResizeMode]}
          onLoadEnd={this.onLoadEnd}
          onError={this.onLoadError}
          onLoadStart={() => {
            // console.log('this is start');
          }}
        />
        {isLoading ? (
          <ActivityIndicator
            style={styles.positionStyle}
            color={Colors.grayShade65}
            size={'small'}
          />
        ) : null}
      </View>
    );
  }

  onLoadEnd = () => {
    // console.log('onLoadEnd');
    this.setState({
      placeHolderShow: false,
      isLoading: false,
    });
  };
  onLoadError = () => {
    // console.log('onLoadError');
    this.setState({ placeHolderShow: true, isLoading: false });
  };
}

AsyncImage.propTypes = {
  source: PropTypes.any,
  style: PropTypes.any,
  onLoadCall: PropTypes.func,
  placeHolder: PropTypes.any,
  resizeMode: PropTypes.any,
  blurRadius: PropTypes.number,
  isUser: PropTypes.bool,
};

AsyncImage.defaultProps = {
  source: '',
  onLoadCall: () => { },
  resizeMode: 'cover',
};
