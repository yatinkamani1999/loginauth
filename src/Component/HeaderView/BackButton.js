/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Image, View, TouchableOpacity, TextInput, StyleSheet, Text } from 'react-native';
// Lib
// import Icon from 'react-native-vector-icons/FontAwesome5Pro';
import PropTypes from 'prop-types';
// Mics
import { Images, Colors, Fonts, Screens, Responsive, Constant } from '../../Theme';
import { drawerOpen, pop, push } from '../../Theme/Actions';
import { strings } from 'Resources/locales/i18n';

export default class BackButton extends Component {
  static propTypes = {
    onPressBack: PropTypes.func,
    title: PropTypes.string,
    style: PropTypes.object,
    showBack: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      isSearchExpand: false,
    };
  }

  onPressBackBtn = () => {
    pop();
  };
  onPressFilter = () => {
    // Constant.commonConstant.TabData = null;
    push(Screens.FilterScreen);
  };
  onPressSearch = (value) => {
    if (value && value.length > 0) {
      const obj = {
        search: value,
      };
      push(Screens.DiscoverQuestionsScreen, { data: obj });
    }
    this.setState({ isSearchExpand: !this.state.isSearchExpand });

  };
  onPressDrawer = () => {
    // Constant.commonConstant.TabData = null;
    drawerOpen();
  };

  render() {
    const {
      title,
      onPressBack,
      style,
      subTitle,
      onPressSubTitle,
      showBack = true,
      isSearchView = false,
      isDiscoverView = false,
      isAppLogo,
      isDrawerShow,
      value,
      onChangeText,
    } = this.props;

    return (
      <View style={style}>
        {isSearchView ? (
          <View style={styles.viewHeader}>
            <View style={styles.sideMenuIcon}>
              {isDrawerShow ? (
                <TouchableOpacity style={styles.drawerIconStyle} onPress={this.onPressDrawer}>
                  <Image source={Images.barIcon} style={{tintColor:Colors.white}} />
                </TouchableOpacity>
              ) : (
                <Icon
                  name={'arrow-left'}
                  size={20}
                  color={Colors.PlaceHolderColor}
                  onPress={onPressBack ? onPressBack : pop}
                />)}
            </View>
            <View>
              {(isDrawerShow && this.state.isSearchExpand) && (
                <View style={styles.searchView}>
                  <TextInput
                    value={value}
                    onChangeText={onChangeText}
                    placeholder={strings('placeHolder.searchHere')}
                    style={styles.inputTextStyle}
                    placeholderTextColor={Colors.white}
                  />
                  {isDrawerShow && (
                    <Icon
                      style={styles.searchIcon}
                      name={'search'}
                      size={20}
                      solid
                      color={Colors.white}
                      onPress={() => { this.onPressSearch(value); }}
                    />
                  )}
                </View>)}
            </View>
            <View style={styles.filterIconStyle}>
              {(isDrawerShow && !this.state.isSearchExpand) && (
                // <Icon
                //   style={styles.searchIcon1}
                //   name={'search'}
                //   size={20}
                //   solid
                //   color={Colors.white}
                //   onPress={() => {
                //     if (this.state.isSearchExpand) {
                //       this.setState({
                //         isSearchExpand: false,
                //       });
                //     } else {
                //       this.setState({
                //         isSearchExpand: true,
                //       });
                //     }
                //   }}
                // />
              )}
              <TouchableOpacity style={styles.filterIcon} onPress={this.onPressFilter}>
                <Image source={Images.Vector} />
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <View style={styles.headerContainer}>
            {/* <Icon
              style={styles.barIconStyle}
              name={'arrow-left'}
              size={20}
              color={Colors.PlaceHolderColor}
              onPress={onPressBack ? onPressBack : pop}
            /> */}
            {isAppLogo && <Image source={Images.titleLogo} />}
            {title && <Text style={styles.titleTextStyle}>{title}</Text>}
            {subTitle && (
              <Text style={styles.subTitleTextStyle} onPress={onPressSubTitle}>
                {subTitle}
              </Text>
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'yellow',
    height: Responsive.heightPercentageToDP(27),
    // marginHorizontal: Responsive.widthPercentageToDP(29),
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // backgroundColor: 'yellow',
    // height: Responsive.heightPercentageToDP(27),
    marginBottom: Responsive.heightPercentageToDP(18),
    paddingLeft:Responsive.widthPercentageToDP(10),
    paddingRight:Responsive.widthPercentageToDP(12),
    width: '100%',
  },
  barIconStyle: {
    height: Responsive.heightPercentageToDP(40),
    width: Responsive.widthPercentageToDP(80),
    paddingStart: Responsive.widthPercentageToDP(29),
    resizeMode: 'contain',
    position: 'absolute',
    left: 0,
    top: 5,
    // backgroundColor: 'pink',
  },
  sideMenuIcon: {
    // position: 'absolute',
    // left: 0,
    width: Responsive.widthPercentageToDP(44),
    height: Responsive.heightPercentageToDP(44),
    justifyContent: 'center',
    // backgroundColor:'pink',
    // alignItems: 'center',
  },
  filterIconStyle: {
    // position: 'absolute',
    // right: 0,
    // backgroundColor: Colors.transparent,
    flexDirection: 'row',
    // width: Responsive.widthPercentageToDP(44),
    // height: Responsive.heightPercentageToDP(44),
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    // borderRadius: 8,
    // backgroundColor: 'pink',
  },
  filterIcon: {
    // position: 'absolute',
    // right: 0,
    backgroundColor: Colors.transparent,
    width: Responsive.widthPercentageToDP(44),
    height: Responsive.heightPercentageToDP(44),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  drawerIconStyle: {
    // position: 'absolute',
    // right: 0,
    backgroundColor: Colors.transparent,
    width: Responsive.widthPercentageToDP(44),
    height: Responsive.heightPercentageToDP(44),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  searchView: {
    width: Responsive.widthPercentageToDP(290),
    height: Responsive.heightPercentageToDP(44),
    paddingHorizontal: Responsive.widthPercentageToDP(15),
    backgroundColor: Colors.searchView,
    opacity: 10,
    borderRadius: 8,
    // justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchIcon: {
    // position: 'absolute',
    // start: 10,
    marginEnd: 5,
    // backgroundColor: Colors.ThemeColorBlue,
    padding: 5,
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  searchIcon1: {
    padding: 10,
    // flexGrow: 1,
    // alignSelf: 'flex-end',
    // justifyContent: 'flex-end',
  },
  inputTextStyle: {
    height: Responsive.heightPercentageToDP(44),
    fontSize: Responsive.convertFontScale(14),
    color: Colors.white,
    fontWeight: '600',
    width: '90%',
    // backgroundColor: 'pink',
  },
  titleTextStyle: {
    fontSize: Responsive.convertFontScale(16),
    color: Colors.blackShade36,
    fontWeight: '800',
  },
  subTitleTextStyle: {
    position: 'absolute',
    right: 0,
    fontSize: Responsive.convertFontScale(14),
    color: Colors.white,
    fontWeight: '800',
  },
});
