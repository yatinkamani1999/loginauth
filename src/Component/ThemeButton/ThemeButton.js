import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
// Mic
import {Colors, Fonts, Responsive} from '../../Theme';

export default class ThemeButton extends Component {
  render() {
    const {textStyle = {}, onPress, textValue, disabled = false} = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        disabled={disabled}
        style={[
          styles.buttonStyle,
          this.props.containerStyle,
          disabled && {backgroundColor: Colors.grayShadeC0},
        ]}
        onPress={onPress}>
        <Text style={styles.textStyle}>{textValue}</Text>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonStyle: {
    width: Responsive.widthPercentageToDP(300),
    height: Responsive.heightPercentageToDP(50),
    backgroundColor: Colors.ThemeColorBlue,
    alignSelf: 'center',
    borderRadius: Responsive.widthPercentageToDP(10),
    justifyContent: 'center',
  },
  textStyle: {
    textAlign: 'center',
    color: Colors.white,
    fontSize: Responsive.convertFontScale(14),
    fontWeight: '700',
  },
});
