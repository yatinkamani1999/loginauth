/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Text, TextInput, View } from 'react-native';
// Lib
// import Icon from 'react-native-vector-icons/FontAwesome5';
// Mics
import { Colors, Fonts, Responsive } from '../../Theme';
import { styles } from './ThemeTextInputStyle';

export default class ThemeTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      showPassword: false,
    };
  }
  // Mics Methods
  onFocus = () => {
    this.setState({ focused: true });
  };

  onBlur = () => {
    this.setState({ focused: false });
  };
  onPressShow = () => {
    const { showPassword } = this.state;
    this.setState({ showPassword: !showPassword });
  };

  // Render Methods
  render() {
    const {
      value,
      icon,
      placeholder,
      onChangeText,
      returnKeyType,
      onSubmitEditing,
      error,
      isPasswordShow,
      autoCapitalize,
      keyboardType,
    } = this.props;
    const { showPassword } = this.state;
    return (
      <View style={{}}>
        <View
          style={[
            styles.container,
            this.props.containerStyle,
            value && { borderColor: Colors.ThemeColorBlue },
          ]}>
          {/* <Icon
            style={styles.inputIcon}
            name={icon}
            size={20}
            solid
            color={value ? Colors.ThemeColorBlue : Colors.PlaceHolderColor}
          /> */}
          <TextInput
            adjustsFontSizeToFit
            value={value}
            onChangeText={onChangeText}
            blurOnSubmit={false}
            icon={''}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            placeholder={placeholder}
            placeholderTextColor={Colors.PlaceHolderColor}
            selectionColor={Colors.ThemeColorBlue}
            returnKeyType={returnKeyType}
            onSubmitEditing={onSubmitEditing}
            ref={input => this.props.inputRef && this.props.inputRef(input)}
            style={[
              styles.inputViewStyle,
              this.props.style,
              isPasswordShow ? { width: Responsive.widthPercentageToDP(208) } : { width: Responsive.widthPercentageToDP(253) },
              value ? {
                fontWeight: '700'
              } : {
                fontWeight: '400'
              },
            ]}
            secureTextEntry={isPasswordShow && !showPassword}
            autoCapitalize={autoCapitalize}
            keyboardType={keyboardType}
            {...this.props}
          />
          {/* {isPasswordShow ? (
            <Icon
              style={styles.eyeIcon}
              name={showPassword ? 'eye' : 'eye-slash'}
              size={20}
              color={
                showPassword ? Colors.ThemeColorBlue : Colors.PlaceHolderColor
              }
              onPress={this.onPressShow}
            />
          ) : null} */}
        </View>
        <Text style={styles.errorText}>{error ? error : ''}</Text>
      </View>
    );
  }
}
