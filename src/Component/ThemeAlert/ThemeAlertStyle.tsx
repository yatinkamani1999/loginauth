import {StyleSheet} from 'react-native';
import {Responsive, Colors, Fonts} from '../../Theme';

export const styles = StyleSheet.create({
  container: {
    padding: 15,
    width: '80%',
    backgroundColor: Colors.white,
    borderRadius: 10,
    alignItems: 'center',
    alignSelf: 'center',
    paddingBottom: 0,
  },
  logoImage: {
    width: Responsive.widthPercentageToDP(150),
    height: Responsive.heightPercentageToDP(40),
    marginBottom: Responsive.heightPercentageToDP(15),
  },
  msgContentStyle: {
    fontSize: Responsive.convertFontScale(15),
    marginBottom: Responsive.heightPercentageToDP(15),
    textAlign: 'center',
  },
  separator: {
    height: 1,
    backgroundColor: Colors.grayShadeE0,
    width: '100%',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
  buttonLabelText: {
    fontSize: Responsive.convertFontScale(15),
    marginVertical: 12,
  },
  buttonView: {flex: 1, justifyContent: 'center'},
  button: {
    alignItems: 'center',
  },
});

export default {styles};
