/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, StatusBar } from 'react-native';
// Lib
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
// Mics
// import { strings } from 'Resources/locales/i18n';
import { Colors, Images } from '../../Theme';
import { styles } from './ThemeAlertStyle';

export default class ThemeAlert extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAlertVisible: false,
      alertMessage: '',
      buttons: [
        {
          id: 1,
          label: 'Ok',
          action: this.hideAlert,
          labelColor: Colors.ThemeColor,
        },
      ],
    };
  }
  // Mics Method
  showAlert = (alertMessage, alertButtons = []) => {
    const { buttons } = this.state;
    this.setState({
      isAlertVisible: true,
      alertMessage,
      buttons: alertButtons.length > 0 ? alertButtons : buttons,
    });
  };

  hideAlert = () => {
    this.setState({
      isAlertVisible: false,
    });
    setTimeout(() => {
      this.setState({
        alertMessage: '',
        buttons: [
          {
            id: 1,
            label: 'Ok',
            action: this.hideAlert,
            labelColor: Colors.ThemeColor,
          },
        ],
      });
    }, 500);
  };

  // Render Methods
  render() {
    const { isAlertVisible, alertMessage, buttons } = this.state;
    return (
      <Modal
        ref={this.props.ref}
        isVisible={isAlertVisible}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        useNativeDriver={true}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.grayShade77} />
          <Image
            style={styles.logoImage}
            source={Images.appLogo}
            resizeMode={'contain'}
          />
          <Text style={styles.msgContentStyle}>{alertMessage}</Text>
          <View style={styles.separator} />
          <View style={styles.buttonContainer}>
            {buttons.map((item, index) => {
              return (
                <View
                  key={index}
                  style={[
                    styles.buttonView,
                    {
                      borderRightColor:
                        buttons.length - 1 === index
                          ? Colors.white
                          : Colors.grayShadeE0,
                      borderRightWidth: buttons.length - 1 === index ? 0 : 1,
                    },
                  ]}>
                  <TouchableOpacity onPress={item.action} style={styles.button}>
                    <Text
                      style={[
                        styles.buttonLabelText,
                        { color: item.labelColor },
                      ]}>
                      {item.label}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        </View>
      </Modal>
    );
  }
}
ThemeAlert.propTypes = {
  ref: PropTypes.any,
};
