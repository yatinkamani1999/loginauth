import {Alert} from 'react-native';
import {Colors} from '../Theme';

import {alertRef} from '../../App';

export function showAlert(message, buttons) {
  setTimeout(() => {
    alertRef.current.showAlert(message, buttons);
  }, 500);
}


export function hideAlert() {
  alertRef.current.hideAlert();
}

export function showNetWorkAlert() {
  let buttons = [
    {
      id: 1,
      label: ("Ok"),
      action: () => {
        hideAlert();
      },
      labelColor: Colors.ThemeColorBlue,
    },
  ];
  setTimeout(() => {
    alertRef.current.showAlert(
      ('Network Not Available'),
      buttons,
    );
  }, 500);
}

export function showAlertWithOkBtn(
  message,
  title = 'Auth',
  buttonTitle = ('Ok'),
) {
  return new Promise(resolve => {
    Alert.alert(title, message, [
      {text: buttonTitle, onPress: () => resolve(''), style: 'cancel'},
    ]);
  });
}

export function showAlertYesNo(
  message,
  title = 'Auth',
  buttonTitleYes = 'Yes',
  buttonTitleNo = 'No',
) {
  return new Promise((resolve, reject) => {
    Alert.alert(title, message, [
      {text: buttonTitleYes, onPress: () => resolve(''), style: 'default'},
      {text: buttonTitleNo, onPress: () => reject(''), style: 'cancel'},
    ]);
  });
}

export function showAlertDeleteCancel(
  message,
  title = 'Auth',
  buttonTitleYes,
  buttonTitleNo,
) {
  return new Promise((resolve, reject) => {
    Alert.alert(title, message, [
      {text: buttonTitleYes, onPress: () => resolve(''), style: 'default'},
      {text: buttonTitleNo, onPress: () => reject(''), style: 'cancel'},
    ]);
  });
}
