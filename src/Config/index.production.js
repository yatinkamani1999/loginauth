import {Constant} from '../Theme';

export default {
  baseURL: 'http://restapi.adequateshop.com',
  socketUrl: 'http://example.com',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${Constant.commonConstant.userToken}`,
  },
  timeOut: 30000,
};
