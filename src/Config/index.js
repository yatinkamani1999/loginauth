import {Constant} from '../Theme';

export default {
  baseURL: 'http://restapi.adequateshop.com',
  socketUrl: 'http://X.X.X.X',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    tenant: 'root',
    Authorization: `Bearer ${Constant.commonConstant.userToken}`,
  },
  timeOut: 30000,
};
