import { registerRootComponent } from 'expo';
import { AppRegistry, LogBox } from 'react-native';

import App from './App';

LogBox.disableYellowBox = true;

LogBox.ignoredYellowBox = ['Warning: Each', 'Warning: Failed']
// import {name as appName} from './app.json';
// AppRegistry.registerComponent(appName, () => App);

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
